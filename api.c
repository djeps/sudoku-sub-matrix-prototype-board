#define F_CPU 16000000UL

#include <avr/io.h>
#include <util/delay.h>

#include "api.h"

// Good values 50..150
#define DIGIT_DELAY 75

void display_digit(uint8_t digit)
{
	_clear_bit(PORTPINS, _select_bit(D3));
	_clear_bit(PORTPINS, _select_bit(D2));
	_clear_bit(PORTPINS, _select_bit(D1));
	_clear_bit(PORTPINS, _select_bit(D0));
	
	switch(digit)
	{
		case 1:
			_set_bit(PORTPINS, _select_bit(D0));
			break;
			
		case 2:
			_set_bit(PORTPINS, _select_bit(D1));
			break;
			
		case 3:
			_set_bit(PORTPINS, _select_bit(D1) | _select_bit(D0));
			break;
			
		case 4:
			_set_bit(PORTPINS, _select_bit(D2));
			break;
			
		case 5:
			_set_bit(PORTPINS, _select_bit(D2) | _select_bit(D0));
			break;
			
		case 6:
			_set_bit(PORTPINS, _select_bit(D2) | _select_bit(D1));
			break;
			
		case 7:
			_set_bit(PORTPINS, _select_bit(D2) | _select_bit(D1) | _select_bit(D0));
			break;
			
		case 8:
			_set_bit(PORTPINS, _select_bit(D3));
			break;
			
		case 9:
			_set_bit(PORTPINS, _select_bit(D3) | _select_bit(D0));
			break;
			
		case 0:
		default:
			break;
	}

	 // Absolutely needed! :(
	 // To low - and all you see is a dimmed 8 across all LEDs
	 // To high - the digits become clearer but there's a noticeable flickering (even blinking!)
	_delay_us(DIGIT_DELAY);
}

void lcd_display(uint8_t lcd, uint8_t num)
{
	switch (lcd)
	{
		case 1:
			_enable_display1();
			display_digit(num);
			_disable_display1();
			break;
		
		case 2:
			_enable_display2();
			display_digit(num);
			_disable_display2();
			break;
		
		case 3:
			_enable_display3();
			display_digit(num);
			_disable_display3();
			break;
			
		case 4:
			_enable_display4();
			display_digit(num);
			_disable_display4();
			break;
			
		case 5:
			_enable_display5();
			display_digit(num);
			_disable_display5();
			break;
			
		case 6:
			_enable_display6();
			display_digit(num);
			_disable_display6();
			break;
			
		case 7:
			_enable_display7();
			display_digit(num);
			_disable_display7();
			break;
			
		case 8:
			_enable_display8();
			display_digit(num);
			_disable_display8();
			break;
			
		case 9:
			_enable_display9();
			display_digit(num);
			_disable_display9();
			break;
		
		default:
			break;
	}
}