#ifndef API_H_
#define API_H_

#include "hal.h"

// Boolean flags
#define TRUE  1
#define FALSE 0

// Modes of operation
#define MODE_AUTOMATIC            0
#define MODE_SINGLE_ACROSS        1
#define MODE_INCREMENTAL          2
#define MODE_INCREMENTAL_REVERSED 3
#define MODE_MANUAL               4

// 7-segment LCD enable pins
#define EN1 PD0
#define EN2 PD2
#define EN3 PD3
#define EN4 PD4
#define EN5 PD5
#define EN6 PD6
#define EN7 PD7
#define EN8 PB0
#define EN9 PB1

// Switches/push-buttons pins
#define SW_UP   PC1
#define SW_SET  PC2
#define SW_DOWN PC3

// CD4511 latch decoder inputs
#define BCD0_VAL1 PB2
#define BCD1_VAL2 PB3
#define BCD2_VAL4 PB4
#define BCD3_VAL8 PB5

#define D0        BCD0_VAL1
#define D1        BCD1_VAL2
#define D2        BCD2_VAL4
#define D3        BCD3_VAL8

// Output pins macros
#define _set_bcd_val_pins_on_port_b_as_outputs() _set_bit( (DDRB), (_select_bit(BCD0_VAL1) | _select_bit(BCD1_VAL2) | _select_bit(BCD2_VAL4) |_select_bit(BCD3_VAL8)) )

#define _set_enable_pins_on_port_d_as_outputs()  _set_bit( (DDRD), (_select_bit(EN1) | _select_bit(EN2) | _select_bit(EN3) | _select_bit(EN4) | _select_bit(EN5) | _select_bit(EN6) | _select_bit(EN7)) )
#define _set_enable_pins_on_port_b_as_outputs()  _set_bit( (DDRB), (_select_bit(EN8) | _select_bit(EN9)) )

// CD4511 latch decoder macros
#define _set_bcd_val_pins()                      _set_bcd_val_pins_on_port_b_as_outputs()

// 7-segment LCD enable pin macros
#define _set_enable_pins() {\
			_set_enable_pins_on_port_d_as_outputs();\
			_set_enable_pins_on_port_b_as_outputs();\
		}

// 7-segment LCD macros
#define PORTPINS PORTB
// LCD1
#define _enable_display1()  _set_bit( (PORTD), (_select_bit(EN1)) )
#define _disable_display1() _clear_bit( (PORTD), (_select_bit(EN1)) )
// LCD1
#define _set_display1()     _enable_display1()
#define _clear_display1()   _disable_display1()
// LCD2
#define _enable_display2()  _set_bit( (PORTD), (_select_bit(EN2)) )
#define _disable_display2() _clear_bit( (PORTD), (_select_bit(EN2)) )
// LCD2
#define _set_display2()     _enable_display2()
#define _clear_display2()   _disable_display2()
// LCD3
#define _enable_display3()  _set_bit( (PORTD), (_select_bit(EN3)) )
#define _disable_display3() _clear_bit( (PORTD), (_select_bit(EN3)) )
// LCD3
#define _set_display3()     _enable_display3()
#define _clear_display3()   _disable_display3()
// LCD4
#define _enable_display4()  _set_bit( (PORTD), (_select_bit(EN4)) )
#define _disable_display4() _clear_bit( (PORTD), (_select_bit(EN4)) )
// LCD4
#define _set_display4()     _enable_display4()
#define _clear_display4()   _disable_display4()
// LCD5
#define _enable_display5()  _set_bit( (PORTD), (_select_bit(EN5)) )
#define _disable_display5() _clear_bit( (PORTD), (_select_bit(EN5)) )
// LCD5
#define _set_display5()     _enable_display5()
#define _clear_display5()   _disable_display5()
// LCD6
#define _enable_display6()  _set_bit( (PORTD), (_select_bit(EN6)) )
#define _disable_display6() _clear_bit( (PORTD), (_select_bit(EN6)) )
// LCD6
#define _set_display6()     _enable_display6()
#define _clear_display6()   _disable_display6()
// LCD7
#define _enable_display7()  _set_bit( (PORTD), (_select_bit(EN7)) )
#define _disable_display7() _clear_bit( (PORTD), (_select_bit(EN7)) )
// LCD7
#define _set_display7()     _enable_display7()
#define _clear_display7()   _disable_display7()
// LCD8
#define _enable_display8()  _set_bit( (PORTB), (_select_bit(EN8)) )
#define _disable_display8() _clear_bit( (PORTB), (_select_bit(EN8)) )
// LCD8
#define _set_display8()     _enable_display8()
#define _clear_display8()   _disable_display8()
// LCD9
#define _enable_display9()  _set_bit( (PORTB), (_select_bit(EN9)) )
#define _disable_display9() _clear_bit( (PORTB), (_select_bit(EN9)) )
// LCD9
#define _set_display9()     _enable_display9()
#define _clear_display9()   _disable_display9()

// Switches macros
#define _set_inputs_port_c()             _clear_bit( (DDRC), (_select_bit(SW_UP)) | (_select_bit(SW_SET)) | (_select_bit(SW_DOWN)) )
#define _enable_internal_pullup_port_c() _set_bit( (PORTC), ( _select_bit(SW_UP)) | (_select_bit(SW_SET)) | (_select_bit(SW_DOWN)) )

#define _up_switch_pressed()             _read_bit( PINC, _select_bit(SW_UP) )
#define _set_switch_pressed()            _read_bit( PINC, _select_bit(SW_SET) )
#define _down_switch_pressed()           _read_bit( PINC, _select_bit(SW_DOWN) )

void display_digit(uint8_t digit);
void lcd_display(uint8_t lcd, uint8_t num);
void resetApiTimer(uint16_t *pTimer);

#endif /* API_H_ */