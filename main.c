#define F_CPU 1000000UL

#define __DELAY_BACKWARD_COMPATIBLE__

#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>

#include "api.h"

volatile uint16_t timer = 0;
volatile uint8_t longPress = FALSE;

int main(void)
{ 
	uint8_t lcd = 0;	
	uint8_t mode = 0;
	
	_set_bcd_val_pins();
	_set_enable_pins();
	
	_set_inputs_port_c();
	
	TCCR0A |= 1 << WGM01;  // Timer 0 mode 2 - CTC
	TCCR0B |= 1 << CS02;   // Set pre-scaler to 256
	OCR0A = 125;           // Number of ticks in Output Compare Register for a delay of 2ms
	TIMSK0 |= 1 << OCIE1A; // Trigger interrupt when counter(TCNT0) >= OCR0A
	
	sei();
	
	if (_set_switch_pressed())
	{
		mode = MODE_SINGLE_ACROSS;
	}
	else if (_down_switch_pressed())
	{
		mode = MODE_INCREMENTAL_REVERSED;
	}
	else if (_up_switch_pressed())
	{
		mode = MODE_INCREMENTAL;
	}
	else if (_up_switch_pressed() && _down_switch_pressed())
	{
		mode = MODE_MANUAL;
	}
	else
	{
		mode = MODE_AUTOMATIC;
	}
	
	
    while (1) 
    {
		if (mode == MODE_AUTOMATIC)
		{
			return 0;
		}
		
		if (mode == MODE_MANUAL)
		{
			
		}
		
		if ((mode == MODE_SINGLE_ACROSS) || (mode == MODE_INCREMENTAL) || (mode == MODE_INCREMENTAL_REVERSED))
		{
			lcd++;
			
			if (mode == MODE_SINGLE_ACROSS)
			{
				lcd_display(lcd, 8);
			}
			
			if (mode == MODE_INCREMENTAL)
			{
				lcd_display(lcd, lcd);
			}
			
			if (mode == MODE_INCREMENTAL_REVERSED)
			{
				lcd_display(lcd, 10 - lcd);
			}
			
			if (lcd == 9)
			{
				lcd = 0;
			}
		}
    }
	
	return 0;
}

ISR(TIMER0_COMPA_vect)
{
	timer++;
	
	if (timer > 1) {
		longPress = TRUE; // A button press > 2ms
	}
	else
	{
		longPress = FALSE;
	}
}

