#ifndef HAL_H_
#define HAL_H_

#define _select_bit( bit )       ( (1) << (bit) )
#define _set_bit( val, mask )    ( (val) |= (mask) )
#define _clear_bit( val, mask )  ( (val) &= (~mask) )
#define _toggle_bit( val, mask ) ( (val) ^= (mask) )
#define _read_bit(val, mask)   (val & mask)

#endif /* HAL_H_ */